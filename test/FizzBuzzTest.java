import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class FizzBuzzTest {

    @Test
    public void tresElementos(){
        int num = 3;
        FizzBuzz fizz = new FizzBuzz();

        String res = fizz.printFizzBuzz(num);

        assertEquals("Fizz",res);
    }
    @Test
    public void elementoDivisibleEnCinco(){
        int num =5;
        FizzBuzz buzz =new FizzBuzz();
        String res= buzz.printFizzBuzz(num);
        assertEquals("Buzz", res );

    }



}