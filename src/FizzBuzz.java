public class FizzBuzz {

    public void printFizzBuzz(){
        for (int i = 1; i <= 100; i++) {
            if (i%3 == 0 && i%5 == 0)
                System.out.println("FizzBuzz");
            else if(i%3 == 0)
                System.out.println("Fizz");
            else if(i%5 == 0)
                System.out.println("Buzz");
            else
                System.out.println(i);
        }
    }

    public String printFizzBuzz(int number){
        String res = "";
        if (number%3 == 0 && number%5 == 0) {
            System.out.println("FizzBuzz");
            res = "FizzBuzz";
        }else if(number%3 == 0) {
            System.out.println("Fizz");
            res = "Fizz";
        }else if(number%5 == 0) {
            System.out.println("Buzz");
            res = "Buzz";
        }else {
            System.out.println(number);
            res = number + "";
        }
        return res;
    }

}
